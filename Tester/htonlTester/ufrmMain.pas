unit ufrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, uByteTools;

type
  {$IF RTLVersion<25}
  IntPtr=Integer;
  {$IFEND IntPtr}
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    btn1: TButton;
    btn2: TButton;
    Button2: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uNetworkTools;

{$R *.dfm}



procedure TForm1.btn1Click(Sender: TObject);
var
  lvByte:array[0..3] of byte;
  l, j, x : cardinal;
  lvPByte:PByte;
begin
  l := $1;
  move(l, lvByte, SizeOf(l));

  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));

  l := $FFFFFFFF;
  move(l, lvByte, SizeOf(l));
  memo1.Lines.Add(intToStr(l));
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));


  Memo1.Lines.Add('=====================');
  l := $FFEEDDCC;
  Memo1.Lines.Add(intToStr(l));
  move(l, lvByte, SizeOf(l));
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));

  j := l shr 24;
  Memo1.Lines.Add(intToStr(j));
  move(j, lvByte, SizeOf(j));
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));

  j := l shr 16;
  Memo1.Lines.Add(intToStr(j));
  move(j, lvByte, SizeOf(j));
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));

  Memo1.Lines.Add('=======bytes========');
  j := l;
  lvByte[0] := (j shr 24);
  lvByte[1] := (j shr 16);
  lvByte[2] := (j shr 8);
  lvByte[3] := (j);
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));


  Memo1.Lines.Add('==========指针========');
  j := l;
  lvPByte := PByte(@j);
  PByte(lvPByte)^ := byte(j shr 24);
  PByte(IntPtr(lvPByte) + 1)^ := byte(j shr 16);
  PByte(IntPtr(lvPByte) + 2)^ := byte(j shr 8);
  PByte(IntPtr(lvPByte) + 3)^ := byte(j);
  Memo1.Lines.Add(TByteTools.varToByteString(lvByte, SizeOf(lvByte)));

end;

procedure TForm1.btn2Click(Sender: TObject);
var
  lvByte:array[0..3] of byte;
  l, j, x : integer;

  c, c1:Cardinal;

  i64_1, i64_2:Int64;
  lvPByte:PByte;
begin
  l := -2;
  Memo1.Lines.Add(TByteTools.varToHexString(l, SizeOf(l)));
  Memo1.Lines.Add(TByteTools.varToHexString(byte(l), SizeOf(byte)));


  Memo1.Lines.Add('===========');
  l := $01234567;
  Memo1.Lines.Add(TByteTools.varToHexString(l, SizeOf(l)));
  Memo1.Lines.Add(TByteTools.varToHexString(byte(l), SizeOf(byte)));



  Memo1.Lines.Add('=======test htonl');
  l := $12345678;
  j := l;
  Memo1.Lines.Add(TByteTools.varToHexString(j, SizeOf(j)));

  j := TByteTools.swap32(l);
  Memo1.Lines.Add(TByteTools.varToHexString(j, SizeOf(j)));
  j := TNetworkTools.htonl(l);
  Memo1.Lines.Add(TByteTools.varToHexString(j, SizeOf(j)));


  Memo1.Lines.Add('=======test cardinal_htonl');
  c := $12345678;
  c1 := c;
  Memo1.Lines.Add(TByteTools.varToHexString(c1, SizeOf(c1)));
  c1 := c;
  c1 := TByteTools.swap32(c1);
  Memo1.Lines.Add(TByteTools.varToHexString(c1, SizeOf(c1)));

  c1 := c;
  c1 := TNetworkTools.htonl(c1);
  Memo1.Lines.Add(TByteTools.varToHexString(c1, SizeOf(c1)));

  Memo1.Lines.Add('=======test int64_htonl');
  i64_1 := $123456789ABCDEFF;
  i64_2 := i64_1;
  Memo1.Lines.Add(TByteTools.varToHexString(i64_2, SizeOf(i64_2)));
  i64_2 := i64_1;
  i64_2 := TByteTools.swap64(i64_2);
  Memo1.Lines.Add(TByteTools.varToHexString(i64_2, SizeOf(i64_2)));

  i64_2 := i64_1;
  i64_2 := TNetworkTools.htonl(i64_2);
  Memo1.Lines.Add(TByteTools.varToHexString(i64_2, SizeOf(i64_2)));

end;

//procedure LE2BE(ASize:Integer);
//  var
//    I,C:Integer;
//    B:Byte;
//  begin
//  C:=ASize shr 1;
//  for I := 0 to C-1 do
//    begin
//    B:=AValue.BArray[I];
//    AValue.BArray[I]:=AValue.BArray[ASize-I-1];
//    AValue.BArray[ASize-I-1]:=B;
//    end;
//  end;

procedure TForm1.Button1Click(Sender: TObject);
var
  l, j:Integer;
  lvPByte: PByte;
begin
  l := $1;
  Memo1.Lines.Add('$1' + ':' + intToStr(l));
  Memo1.Lines.Add(TByteTools.varToByteString(l, SizeOf(l)));
  Memo1.Lines.Add(TByteTools.varToHexString(l, SizeOf(l)));

  Memo1.Lines.Add('');


  l := $AABBCCFF;
  Memo1.Lines.Add('$AABBCCFF' + ':' + intToStr(l));
  Memo1.Lines.Add(TByteTools.varToByteString(l, SizeOf(l)));
  Memo1.Lines.Add(TByteTools.varToHexString(l, SizeOf(l)));

  Memo1.Lines.Add('');

  j := l;
  j := TNetworkTools.htonl(j);
  Memo1.Lines.Add(TByteTools.varToByteString(j, SizeOf(j)));
  Memo1.Lines.Add(TByteTools.varToHexString(j, SizeOf(j)));

  Memo1.Lines.Add('');
  
  j := l;
  lvPByte := PByte(@j);
  PByte(lvPByte)^ := byte(j shr 24) AND $FF;
  PByte(IntPtr(lvPByte) + 1)^ := byte(j shr 16) AND $FF;
  PByte(IntPtr(lvPByte) + 2)^ := byte(j shr 8) AND $FF;
  PByte(IntPtr(lvPByte) + 3)^ := byte(j) AND $FF;      

  Memo1.Lines.Add(TByteTools.varToByteString(j, SizeOf(j)));
  Memo1.Lines.Add(TByteTools.varToHexString(j, SizeOf(j)));



//      AValue.BArray[0]:=(iVal shr 24) and $FF;
//      AValue.BArray[1]:=(iVal shr 16) and $FF;
//      AValue.BArray[2]:=(iVal shr 8) and $FF;
//      AValue.BArray[3]:=iVal and $FF;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  s2:string;
  aStr:AnsiString;
  i:Integer;
  P:Pointer;
begin
  s2:= Chr(167) + 'a';
  Memo1.Lines.Add(s2 + '(wideString):' + TByteTools.varToByteString(s2[1], Length(s2) * SizeOf(Char)));
  Memo1.Lines.Add(s2 + '(wideString):' + TByteTools.varToHexString(s2[1], Length(s2) * SizeOf(Char)));

  p:=PAnsiChar(AnsiString(s2));
  Memo1.Lines.Add(s2 + '(ansiString):' + TByteTools.varToByteString(p^, Length(s2) * SizeOf(Char)));
  Memo1.Lines.Add(s2 + '(ansiString):' + TByteTools.varToHexString(p^, Length(s2) * SizeOf(Char)));

  p:=PAnsiChar(AnsiString(UTF8Encode(s2)));
  Memo1.Lines.Add(s2 + '(utf8):' + TByteTools.varToByteString(p^, Length(s2) * SizeOf(Char)));
  Memo1.Lines.Add(s2 + '(utf8):' + TByteTools.varToHexString(p^, Length(s2) * SizeOf(Char)));

  s2 := '汉';
  p:=Pointer(s2[1]);
  Memo1.Lines.Add(s2 + '(wideString):' + TByteTools.varToByteString(p^, Length(s2) * SizeOf(Char)));
  Memo1.Lines.Add(s2 + '(wideString):' + TByteTools.varToHexString(p^, Length(s2) * SizeOf(Char)));

  aStr := AnsiString(s2);
  p:=PAnsiChar(aStr);
  Memo1.Lines.Add(s2 + '(ansiString):' + TByteTools.varToByteString(p^, Length(aStr)));
  Memo1.Lines.Add(s2 + '(ansiString):' + TByteTools.varToHexString(p^, Length(aStr)));

  aStr := UTF8Encode(s2);
  p:=PAnsiChar(aStr);
  Memo1.Lines.Add(s2 + '(utf8):' + TByteTools.varToByteString(p^, Length(aStr)));
  Memo1.Lines.Add(s2 + '(utf8):' + TByteTools.varToHexString(p^, Length(aStr)));
end;

end.

//10100001 11101100
//
// 11000010 10100001
//Unicode编码(十六进制)　
//UTF-8 字节流(二进制)
//000000 - 00007F  0xxxxxxx
//000080 - 0007FF  110xxxxx 10xxxxxx
//000800 - 00FFFF  1110xxxx 10xxxxxx 10xxxxxx
//010000 - 10FFFF  11110xxx 10xxxxxx 10xxxxxx 10xxxxxx

